import React from 'react';
import { Card, CardTitle } from 'material-ui/Card';
import Map from "./Map";
import About from "./AboutMe.jsx";
import FontIcon from 'material-ui/FontIcon';
import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';
import {Tabs, Tab} from 'material-ui/Tabs';
import Slider from 'material-ui/Slider';
import Drop from "./DropDownList.jsx"

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

function handleActive(tab) {
  alert(`A tab with this route property ${tab.props['data-route']} was activated.`);
}

const HomePage = () => (
<Card className="container">
  	<Tabs>
    	<Tab label="User List" >
    		<div>
        		<Drop/>
        		<Slider name="slider0" defaultValue={0.5} />
      		</div>
    	</Tab>
    	<Tab label="About Me" >
      		<div>
        		<h2 style={styles.headline}>` Меня зовут Евстафиев Юрий и я:`</h2>
        		<p>
          			<About/>
        		</p>
      		</div>
    	</Tab>
  	</Tabs>
</Card>
);

export default HomePage;