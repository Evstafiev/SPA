import React, {Component} from 'react';
import {compose, withProps} from "recompose";
import {GoogleMap, Marker, withGoogleMap, withScriptjs} from "react-google-maps";

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markers: []
        };
    }

    mapClicked(mapProps) {
        let {lat, lng} = mapProps.latLng;
        this.setState({
            markers: [...this.state.markers, {lat: lat(), lng: lng()}]
        });
    }

    render() {
        const {markers} = this.state;
        return (
            <GoogleMap
                defaultZoom={12}
                defaultCenter={{lat: 46.408683, lng: 30.74}}
                onClick={(event) => this.mapClicked(event)}>
                {markers.map(({ lat, lng }, index) =>
                    <Marker
                        key={index}
                        position={{ lat: +lat, lng: +lng }}
                    />)}
            </GoogleMap>
        );
    }
}

const MapContainer = compose(
    withProps({
        googleMapURL:
            "https://maps.googleapis.com/maps/api/js?key=AIzaSyC5poCPZvuhMcqv3NzmUviR521XFT3e9FE&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{height: `40%`}}/>,
        containerElement: <div style={{height: `50vh`}}/>,
        mapElement: <div style={{height: `100%`}}/>
    }),
    withScriptjs,
    withGoogleMap
)(Map);

export default MapContainer;