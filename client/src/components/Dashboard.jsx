import React, { PropTypes } from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Map from "./Map";
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
const style = {
  margin: 12,
};
const Dashboard = ({ secretData }) => (
  <Card className="container">
    <CardTitle
      title="Personal Account"
      subtitle="Here you can add your places to the map."
    />
    	<Map/>
    	<br />
    <TextField id = "adress"
      floatingLabelText="Write down the adress"
    /><br />
    <TextField id = "Name"
      floatingLabelText="What is that place?"
    /><br />
  	<RaisedButton label="Add" primary={true} style={style} onclick='saveData()' />
    {secretData && <CardText style={{ fontSize: '16px', color: 'green' }}>Only authorized users can see this page.</CardText>}
  </Card>
);

Dashboard.propTypes = {
  secretData: PropTypes.string.isRequired
};
 	  let map;
      let marker;
      let infowindow;
      let messagewindow;

      function saveData() {
        let name = escape(document.getElementById('name').value);
        let address = escape(document.getElementById('address').value);
        let type = document.getElementById('type').value;
        let latlng = marker.getPosition();
        let url = 'phpsqlinfo_addrow.php?name=' + name + '&address=' + address +
                  '&type=' + type + '&lat=' + latlng.lat() + '&lng=' + latlng.lng();

        downloadUrl(url, function(data, responseCode) {

          if (responseCode == 200 && data.length <= 1) {
            infowindow.close();
            messagewindow.open(map, marker);
          }
        });
      }

      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request.responseText, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing () {
      }
export default Dashboard;

