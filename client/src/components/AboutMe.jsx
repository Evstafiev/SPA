import React, {Component} from 'react';
import { Card, CardTitle } from 'material-ui/Card';

class About extends Component {
    
    render() {

        return (
                <ul>
                    <li>Учусь в ОНАЗ им О.С. Попова, на кафедре ИК.</li>
                    <br />
                    <li>Закончил курсы Front-End Pro в компьютерной школе Hillel.</li>
                    <br />
                    <li>Работал 6 месяцев на должности младшего системного администратора.</li>
                    <br />
                    <li>Апнул 4К ММР в доте.</li>
                    <br />
                    <li>Считаю что "Звездные войны - новая надежда" лучшая часть оригинальной трилогии.</li>
                    <br />
                </ul>
        )
    }
};

export default About;