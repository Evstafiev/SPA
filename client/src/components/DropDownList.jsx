import React, {Component} from 'react';
import Map from './Map'
import {Tabs, Tab} from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';
import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';

const Drop = () => (
  	<Tabs>
    	<Tab
    	icon={<MapsPersonPin />}
    	label="TestUSER1">
    	  	<Map/>
    	</Tab>
    	<Tab
    	icon={<MapsPersonPin />}
    	label="TestUSER2">
    	  	<Map/>
    	</Tab>
    	<Tab
    	icon={<MapsPersonPin />}
    	label="TestUSER3">
    	  	<Map/>
    	</Tab>
    	<Tab
    	icon={<MapsPersonPin />}
    	label="TestUSER4">
    	  	<Map/>
    	</Tab>
  	</Tabs>
);

export default Drop;